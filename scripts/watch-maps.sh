#!/usr/bin/env sh

cd $(realpath $(dirname $0)/..)
wd=$(pwd)
cd maps

update_maps() {
        for port in $@; do
                $wd/templates/config/vote-maps.py > $wd/servers/$port/config/vote-maps.cfg
                if [ -e "$wd/servers/$port/fifo" ]; then
                        echo "exec config/rst.cfg" > $wd/servers/$port/fifo
                fi
        done
}

if [ "" = "$1" ]; then
	echo "usage: $(basename $0) <port>"
	exit
fi

while true; do
        inotifywait -e modify,close_write,move,create,delete -r $wd/maps
        update_maps $@
        sleep 1
done

