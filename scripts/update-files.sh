#!/usr/bin/env bash

PATH+=:$(realpath $(dirname $0))/bash-tpl

wd=$(realpath $(dirname $0)/..)
cd $wd/templates/

[ -e $wd/server.conf ] && . $wd/server.conf

for port in $@; do
	echo "Build port: $port"
	find -type f -iname '*.tpl' -printf '%P\0' |
		while IFS= read -r -d '' file; do
			echo -en "  parse: \e[32m$file\e[m "
			mkdir -p $(dirname $wd/servers/$port/$file)
			cd $(dirname $file)
			. <(bash-tpl $(basename $file))> $wd/servers/$port/${file%.*}.cfg
			echo -e "-> \e[33m$port/${file%.*}.cfg\e[m"
			cd $wd/templates/
		done
	if [ ! -e $wd/servers/$port/maps ]; then
		echo -e "  link: \e[32mservers/$port/maps/\e[m -> \e[33mservers/$port\e[m"
		ln -rs $wd/maps $wd/servers/$port/
	fi
done



