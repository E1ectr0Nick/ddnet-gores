# What is
DDNet server toolkit and predefined configuration.

# Run

## Prepare
* Copy repository
```sh
  git clone --recurse-submodules https://gitlab.com/E1ectr0Nick/ddnet-gores
  sudo mv ddnet-gores /srv/
  cd /srv/ddnet-gores
```

* Make sure `shareGroup` exists if you want to access files along with the service and the current user.
```sh
  usermod -aG shareGroup %username%
  chown :shareGroup .
  chmod +s . maps
```
Also don't forget modify group in `ddnet-gores@.service`:
```
  Group = shareGroup
```

* Add maps and generate configuration files
```sh
  cp -r <maps> .
  ./scripts/update-files.sh 8303
```

## Launch
* Manually
```sh
  cd servers/8333
  DDNet-Server
```
You may need to explicitly specify the start option: `-f autoexec_server.cfg`.

* Service
Before run service, you must be create user and group `teeworlds`:
```sh
  sudo useradd -r teeworlds
  sudo systemctl link $(realpath ddnet-gores@.service)
  sudo systemctl start ddnet-gores@8303.service
```

* After starting the server, you can also start the maps file monitor:
```sh
./scripts/watch-maps.sh 8303
```
For update `config/vote-maps.cfg` automatically when files updated in `maps` directory.

## FIFO
Create `/etc/sysctl.d/protect-links.conf` with the contents:
```
fs.protected_fifos = 0
```

Then restart procps:
```sh
sudo systemctl restart procps.service
```
See: https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1875225

# Known Bugs
## Can't remove shared ro nested directories
```sh
sudo groupadd share
sudo useradd -G share user1
sudo useradd -G share user2
sudo -u user1 -g share mkdir -m+s,g+rw foo
sudo -u user1 -g share chown :share
cd foo
sudo -u user2 -g share mkdir -mg-w -p bar/baz
sudo -u user1 -g share rm -rf bar #error: rm: cannot remove 'bar/baz/': Permission denied
````

