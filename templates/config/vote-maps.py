#!/usr/bin/env python3

import os
import sys

paths = sys.argv[1:] or ['.']
 
def ls(path):
    if not hasattr(ls, 'counter'):
        ls.counter = 0
    for filename in sorted(os.listdir(path)):
        f = os.path.join(path, filename)
        if os.path.isfile(f):
            rf = os.path.splitext(os.path.relpath(f))[0]
            sf = os.path.splitext(filename)[0]
            vote = 'add_vote "%s" "change_map \\"%s\\""' % (sf, rf)
            print(vote)
        else:
            ls.counter += 1
            sf = os.path.splitext(filename)[0]
            vote = 'add_vote "%s" "info"\n' % (' '*ls.counter)
            vote += 'add_vote "─── %s ───" "info"' % sf.upper()
            print(vote)
            ls(f)

for path in paths:
    if os.path.exists(path):
        ls(path)

